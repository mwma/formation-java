package fr.casden.formation;

import java.util.Map;

import fr.casden.model.Boisson;

public class Distributeur {

    private Boisson boissonSelectionne;
    private double sommeIntroduite=0;
    private Map<String , Boisson> listeBoissons;



    public Distributeur(Map<String, Boisson> listeBoissons) {
        this.listeBoissons = listeBoissons;
    }
    
    public  void choisirBoisson (String nomBoisson) {
        if (listeBoissons.containsKey(nomBoisson)) {
            this.boissonSelectionne=listeBoissons.get(nomBoisson);
            Utils.out("La boisson existe");
            Utils.out("La boisson choisi  : " + boissonSelectionne.getlibelle() + " Merci d'introduir " + boissonSelectionne.getTarif());
        }
        else {
            Utils.out("La boisson selectionnée n'exite pas ");
        }
    }
    public void introduirePaiement (double d) {
        sommeIntroduite=sommeIntroduite+d;
        if (sommeIntroduite >= boissonSelectionne.getTarif()) {
            DistribuerBoisson();
            rendreMonnaie();
            boissonSelectionne=null;
        }
        else {

            System.out.printf("Il faute encore introduire %f \n", boissonSelectionne.getTarif() - sommeIntroduite);
        }
    }
    
    public void rendreMonnaie() {
        double monnaieARendre=sommeIntroduite-boissonSelectionne.getTarif();
        if (monnaieARendre>0) {
            Utils.out("Recuperer votre monnaie :" + monnaieARendre);
        }
        sommeIntroduite=0;
    }
    public  void DistribuerBoisson () {
        Utils.out("Distrubution de la boisson : " + boissonSelectionne.getlibelle() + "  au tarif de : " + boissonSelectionne.getTarif());

    }
}

