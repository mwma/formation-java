package fr.casden.formation;

import java.util.HashMap;

import fr.casden.model.Boisson;

public class App 
{
    
    public static void main( String[] args )  {

        HashMap<String, Boisson> listeBoissons =new HashMap<>();
        Distributeur distribBoisson= new Distributeur(listeBoissons);
        listeBoissons.put("Cafe",new Boisson("Cafe", 1.2d) );
        listeBoissons.put("Biere",new Boisson("Biere", 1.8d) );
        listeBoissons.put("Eau",new Boisson("Eau", 1.0d) );
     
       
            

     distribBoisson.choisirBoisson("Cafe");
     distribBoisson.introduirePaiement(5);
     
    }
}


