package fr.casden.model;

public class Boisson {
    private String libelleBoisson;
    private double tarifBoisson;

    public  Boisson (String libelleBoisson,double tarifBoisson ) {
        this.libelleBoisson=libelleBoisson;
        this.tarifBoisson=tarifBoisson;
    }
    public String getlibelle() {
        return libelleBoisson.toUpperCase();
    }
   public double getTarif() {
        return tarifBoisson;
    }
}
