package fr.casden.model;

public class Utilisateur {
    private String nom;
    private String prenom;
    private int age;

    public  Utilisateur (String nom, String prenom, int age) {
        this.nom=nom;
        this.prenom=prenom;
        this.age=age;
    }
    public String getnom() {
        return nom.toUpperCase();
    }
    public String getprenom() {
        return prenom;
    }
    public int getage() {
        return age;
    }
/*
    public void setage(int age) {
        this.age=age;
    }
*/
    public int vieillir (int nbannees) {
            if (nbannees <= 0) {
                return age;
            }
            if (age+nbannees > 100) {
                age=100;
                return 100;
            }
            age=age+nbannees;
            return age;

    }

}
